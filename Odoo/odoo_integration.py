import csv
import xmlrpc.client

# Lecture des données depuis un fichier CSV
with open(r'C:\Users\Ziwo\Downloads\ODOO.csv', 'r') as f:
    reader = csv.reader(f)
    fields_row = next(reader)
    data_row = next(reader)

# Préparation des données pour Odoo
employee_data = {fields_row[i]: data_row[i] for i in range(len(fields_row))}

# Connection à Odoo
url = 'https://unige.odoo.com'
db = 'unige'  # le nom de la base de données
username = 'nallaoua1999@hotmail.com'  # le nom d'utilisateur Odoo
password = 'test1234'  # le mot de passe Odoo

# Établir une connexion à l'API Odoo
common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
uid = common.authenticate(db, username, password, {})
models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))

# Création d'un nouvel employé dans Odoo
employee_id = models.execute_kw(db, uid, password, 'hr.employee', 'create', [employee_data])

print(f"L'employé a été créé avec l'ID : {employee_id}")
